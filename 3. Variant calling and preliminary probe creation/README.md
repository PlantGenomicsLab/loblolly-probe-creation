  Variant calling and preliminary probe creation
  -------------------
Variant discovery was performed with FreeBayes (v1.0.2) on the five source cohorts independently prior to creating a merged data set. FreeBayes was chosen because the Bayesian framework is designed to detect rare variants (increased sensitivity), consider multi-mapping reads, and also accommodate variable ploidy (haploid and pooled designs). For the standard quality data set, variant discovery required minimum coverage of 8, minimum alternative allele count of 2, base quality of 15, and no threshold for mapping quality. The strict quality variants required a minimum coverage of 10, alternative coverage of 6, mapping quality of 20, and base quality of 25. Both runs allowed SNPs and indels, but no complex events, and required a minimum within-sample minor allele frequency of 0.2.

- Freebayes.sh : Commands for variant discovery.

SNPeff (v4.3q) was used to annotate the candidate variants. A custom database was built from the public loblolly genome (https://treegenesdb.org/FTP/Genomes/Pita/v2.01/genome/) and annotation (https://treegenesdb.org/FTP/Genomes/Pita/v2.01/annotation/). Only probes that have a genome coordinate are capable of annotation. Therefore 1,656 non-aligning Illumina Infinium probes are not included. See publication for statistics on variant annotation.  

For k-mer alignment testing (see next section), probes consisting of 35-nt flanks around strict quality variants were generated in reference sequence (71 nt total).

- 70_mer_probes_just_print.pl : Script that takes in the two quality merged/annotated vcf files and produces probes. Coordinates take the reference base. 

- head_scored_SNPs.txt : First 5000 lines of the scored SNPs output created by 70_mer_probes_just_print.pl. The whole file has as many lines as standard variants. This table provides a wholistic view of variants and is easier to parse than a vcf. It provides the distance on the left and right side to the nearest variant or scaffold edge, variant quality (PASS = strict quality and no variants within the flanking sequence), and additional information useful for probe creation. 
 
- head_SNP_probes.txt: First 5000 lines of probes printed in reference sequence (35-mer sequence flanking the reference allele). There are as many lines as strict bi-allelic quality variants with no N-bases.


Inclusion of the 10 WGS samples
--
WGS are processed identically to the exome capture and ddRAD cohorts. To eliminate spurious heterozygous calls, variants are removed that are called as heterozygous by Freebayes (0/1 annotation) in at least one of the 10 samples. This recudes the number of strict quality SNPs to 557,204. Flanking polymorphism filtering as described in section #6 further filters the WGS variants to 156,456.

Vcf files are linked in section 1.
 
