# Variant calling commands:

# SNP and indel calling is always done with Freebayes (version 1.0.2). Two types of vcf files are produced, one run with standard, generic setting, and another with more stringent settings. This is to have high quality SNPs to build probes around while considering other SNPs in the surrounding sequence.

        # Sample commands:
        freebayes -f genome/pita.v2.0.1.masked.3k.fasta  -u -m 0 -q 15 -F .2 --min-coverage 8 -C 2 sorted_bam/SRR3540314.bam > vcf/standard_SRR3540314.vcf
        freebayes -f genome/pita.v2.0.1.masked.3k.fasta  -u -m 20 -q 25 -F .2 --min-coverage 10 -C 6 sorted_bam/SRR3540314.bam > vcf/strict_SRR3540314.vcf

        # VCF files are merged with bcftools merge by their respective set and then merged into a super set of all strict and all standard variants. 