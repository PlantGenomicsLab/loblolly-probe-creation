#!usr/bin/perl
########################################################################################################################################
# Part 2 : Using the scoring information to build probes
# This script is very similar to the original 70_mer_probes.pl. Therefore, I will only annotate what is different. 
# PRINTS WITH IUPAC THIS TIME! 

open (FASTA_TABLE, "/UCHC/LABS/Wegrzyn/ConiferGenomes/Pita/Freebayes_test/genome/pita.v2.0.1.masked.3k.table")
        or die "CANNOT OPEN FASTA TABLE!!!";
	while ($line = <FASTA_TABLE>){
        $line =~ /\>(.+)/;
        @fasta_table = split "\t", $1;
        $fasta{$fasta_table[0]} = $fasta_table[1];			# scaffold : scaffold sequence
	
}
close FASTA_TABLE;

open (SCORED_SNPS, "scored_SNPs_mapping_adjusted.txt")      # Input adjusted SNP scores from KMER
	or die "CANNOT OPEN SCORED SNPS FILE!!!";

while ($line = <SCORED_SNPS>){
	@split = split "\t", $line;
	$ID = $split[0] . "_" . $split[1];

	$Position_to_IUPAC{$ID} = $split[12];
}

close SCORED_SNPS;
open (SCORED_SNPS, "scored_SNPs_mapping_adjusted.txt")
        or die "CANNOT OPEN SCORED SNPS FILE!!!";
open (PROBES_OUTFILE, ">SNP_probes.txt")
        or die "CANNOT CREATE PROBES OUTPUT!!!";

print PROBES_OUTFILE "Sequence left", "\t", 	
	"SNP", "\t", 
	"Sequence right", "\t", 
	"Scaffold", "\t", 
	"Position", "\n";

while ($scores = <SCORED_SNPS>){
	$scores =~ s/\s+$//;
	if ($scores =~ /STRICT/){
        	@pass = split "\t", $scores;				
        	@sequence = split "", $fasta{$pass[0]};			# Break up the sequence of the matching scaffold.

                        $more = $pass[1] + 34;
                        $start = $pass[1];
                        $less = $pass[1] - 36;
                        $stop = $pass[1]- 2;
                        $current = $pass[1] -1;

			# TAKING INDELS INTO CONSIDERATION

			@split_for_indel = split /\//, $pass[11];					# Split up the XXXXX/XX array
			@split_ref = split "", $split_for_indel[0];					# How big is the first part (>1 = indel)

			if (scalar @split_ref > 1){
				$spaces_to_add = scalar @split_ref - 1;					#1234567/12 means I must add 6 spaces to back end start
				$start = $start + $spaces_to_add;
				$more = $more + $spaces_to_add;
			}

                        for ($a = $less; $a <= $stop; $a++){						# Left base probe printing
				$position = $a + 1;
				$Unique_ID = "$pass[0]_$position";
				
				if ($a > 1){
				if ($Position_to_IUPAC{$Unique_ID}){					
					print PROBES_OUTFILE $Position_to_IUPAC{$Unique_ID};		# If the position matches a IUPAC made before, print it!
				}
				else{
                                	print PROBES_OUTFILE $sequence[$a];
                        	}
				}
			}

                        print PROBES_OUTFILE "\t", $pass[11], "\t";					# This is the actual SNP.

                        for ($a = $start; $a <= $more; $a++){						# Same thing, but now right end of probe.
                                $position = $a + 1;
                                $Unique_ID = "$pass[0]_$position";

				if ($a > 1){
                        	if ($Position_to_IUPAC{$Unique_ID}){
                                        print PROBES_OUTFILE $Position_to_IUPAC{$Unique_ID};
                                }
                                else{
                                        print PROBES_OUTFILE $sequence[$a];
                                }
				}

			}

                        print PROBES_OUTFILE "\t", $pass[0], 						# extra information
				"\t", $pass[1],
				"\t", $pass[12], 
				"\t", $pass[13], 
				"\t", $pass[14], 
				"\t", $pass[15],
				"\n";

                        $more = 0;
                        $less = 0;
                        $start = 0;
                        $stop = 0;

	}
}


close PROBES_OUTFILE;
close SCORED_SNPS;

