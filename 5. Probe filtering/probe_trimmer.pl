#!usr/bin/perl
# PROBE TRIMMER: Takes in probes from the filtering step and cuts at the polymorphism on one arm.
# Run as:
#   perl probe_trimmer.pl [filterd probes outfile] > [trimmed probes outfile]

open (PROBES, $ARGV[0]) or die "Cannot open probes!!!";

while ($line = <PROBES>){
        $line =~ s/\s+$//;
        #print $line, "\n";
        @split = split "\t", $line;

if ($split[7] !~ /./){
        $SNP++;
        @split_left = split "", $split[0];
        @split_right = split "", $split[2];
        for ($a = scalar @split_left - 1; $a >= 0; $a = $a - 1){
                if ($split_left[$a] =~ /A|T|G|C/i){
                        unshift @array_l, $split_left[$a];
                }
                last if ($split_left[$a] !~ /A|T|G|C/i);                        # Stop compiling the probe if we hit a polymorphism <--- SNP direction
        }
        print @array_l, "\t";

        print $split[1], "\t";

        for ($a = 0; $a < scalar @split_right; $a = $a + 1){
                #print $split_left[$a], " ";
                if ($split_right[$a] =~ /A|T|G|C/i){
                        push @array, $split_right[$a];              
                }
                last if ($split_right[$a] !~ /A|T|G|C/i);                        # Stop compiling the probe if we hit a polymorphism SNP ---> direction
        }
        print @array, "\t";
        $number = sprintf("%06d", $SNP);
        $ID = "PitaSNP" . $number;                                              # Renaming the probes in $SNP++ way. 
        
        # Print format (see sample):
        print @array_l, "\[", $split[1], "\]", @array, "\t", $split[3], "\t", $split[4], "\t", $split[5], "\t", $split[7], "\t", $ID, "\n";

        undef (@array);
        undef (@array_l);

}
}
