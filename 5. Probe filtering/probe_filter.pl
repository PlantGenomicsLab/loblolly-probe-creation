#!usr/bin/perl
# PROBE FILTER: Filters probes retaining only those with no polymorphisms in one flanking arm

# Run as:
#   perl probe_filter.pl [input_probes] > [filtered_probes outfile] 

open (INFILE, $ARGV[0]) or die "Cannot open input file!!!";

PART: while ($line = <INFILE>){
        $first = 0;
        $back = 0;
        $line =~ s/\s+$//;
        @split = split "\t", $line;
        @left = split "", $split[0];
        @right = split "", $split[2];

        if (scalar @left < 35){                                                         # size check (sometimes it hits a scaffold end)
                        next;
                }
        if (scalar @right < 35){
                        next;
                }
        if ($split[0] =~ /N/ or split[2] =~ /N/){                                       # No N bases in flank
                        next;
        }
        if ($split[0] =~ /n/ or split[2] =~ /n/){                                       # No n bases in flank
                        next;
        }
         if ($split[0] =~ /i/ or split[2] =~ /i/){                                       # No indels in flank
                        next;
        }
         if ($split[0] =~ /I/ or split[2] =~ /I/){                                       # No indels in flank
                        next;
        }
        foreach $base (@left){
                        if ($base =~ /A|C|T|G/i){
                                $count_R++;                    
                        }
        }
        foreach $base (@right){
                        if ($base =~ /A|C|T|G/i){
                            $count_L++;                       
                        }
        }
        
        if ($count_L == 35 or $count_R == 35){                                  # One flank should have no polymorphisms/indels
                print $line, "\n";
                $count_R = 0;
                $count_L = 0;
                next PART;
        }
                $count_R = 0;
                $count_L = 0;
}
close INFILE;


