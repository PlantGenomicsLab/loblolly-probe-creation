# BWA mem alignment of reads to the pita 2.01 genome

# Alignment is always done using BWA (0.7.15) and all reads are aligned against the 2.01 3k version of the Pita Genome.

        # Sample command:
                bwa mem \
                -t 32 \
                genome/pita.v2.0.1.masked.3k \
                PRJNA320114_TAMU/trimmed_fastq/trimmed_SRR3540310_1.fastq.gz \
                PRJNA320114_TAMU/trimmed_fastq/trimmed_SRR3540310_1.fastq.gz \
                > sam/SRR3540310.sam
                
        # Repeated for each individual sample

       # Once alignment sam files are produced, they are converted to bam and then sorted using samtools (v1.3.1).

        # Sample commands:
               samtools view -@ 32 -b -o bam/SRR3540310.bam sam/SRR3540310.sam
               samtools sort -@ 32 -o sorted_bam/SRR3540310.bam bam/SRR3540310.bam