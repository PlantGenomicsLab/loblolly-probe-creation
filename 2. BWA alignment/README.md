Alignment and conversion
---------------------------------

BWA_align_and_samtools_conversion.sh : Sample commands for alignment of trimmed reads to the genome and samtools conversion. 

The 3,021 illumina infinium probes were aligned to the loblolly pine reference genome (v2.01) with Bowtie2 (2.3.3.1). Bowtie was chosen due to the explicit ability to handle ambiguous bases sequences to be aligned. Probes were transformed into a fasta format with the central SNP in IUPAC form.

Command:
```
    $ bowtie2-align-l --wrapper basic-0 -f final_formatted_probes_for_alignment.fasta \
    -p 16 -x /UCHC/LABS/Wegrzyn/ConiferGenomes/Pita/pita.v2.0.1.masked3k2/pita.v2.0.1.masked3k2_new \
    -S final_formatted_probes_for_alignment.sam
```


Statistics on alignment are available here: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/CTGN/alignment_stats.txt.gz

All samples were aligned to the loblolly reference genome (v2.01) which can be accessed here: https://treegenesdb.org/FTP/Genomes/Pita/v2.01/

Links to bam files are available in previous section.
