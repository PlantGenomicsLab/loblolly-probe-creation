Details for NCSU samples from the Master's thesis of Laura Ashley Townsend (under Ross Whetten at NCSU):

 Each of the libraries were sequenced for 100bp paired end reads over 4 flowcell lanes (~96 samples/lane) on an Illumina HiSeq2000 using V3 chemistry. A total of 16 flowcell lanes were sequenced by the Beijing Genomics Institute service center in Philadelphia. Here are the 16 lanes:
 
                Ar1_FCC21M0ACXX_s_5_fastq
                Ar1_FCC21WAACXX_s_7_fastq
                Ar1_FCD23ETACXX_s_5_fastq
                Ar1_FCD23ETACXX_s_6_fastq
                Ar2_FCC21M0ACXX_s_5_fastq
                Ar2_FCC21WAACXX_s_7_fastq
                Ar2_FCD23ETACXX_s_5_fastq
                Ar2_FCD23ETACXX_s_6_fastq
                Br1_FCC21M0ACXX_s_1_fastq
                Br1_FCC21WAACXX_s_8_fastq
                Br1_FCD23ETACXX_s_7_fastq
                Br1_FCD23ETACXX_s_8_fastq
                Br2_FCC21M0ACXX_s_1_fastq
                Br2_FCC21WAACXX_s_8_fastq
                Br2_FCD23ETACXX_s_7_fastq
                Br2_FCD23ETACXX_s_8_fastq
                Cr1_FCC21M0ACXX_s_2_fastq
                Cr1_FCD23J4ACXX_s_1_fastq
                Cr1_FCD23J4ACXX_s_2_fastq
                Cr1_FCD23J4ACXX_s_3_fastq
                Cr2_FCC21M0ACXX_s_2_fastq
                Cr2_FCD23J4ACXX_s_1_fastq
                Cr2_FCD23J4ACXX_s_2_fastq
                Cr2_FCD23J4ACXX_s_3_fastq
                Dr1_FCD23J4ACXX_s_4_fastq
                Dr1_FCD23J4ACXX_s_5_fastq
                Dr1_FCD23J4ACXX_s_6_fastq
                Dr2_FCD23J4ACXX_s_4_fastq
                Dr2_FCD23J4ACXX_s_5_fastq
                Dr2_FCD23J4ACXX_s_6_fastq

 Each starting letter is the pool and the 1 or 2 in Ar1 or Ar2, for example, specifies the forward and reverse.
 
  These are in 6nt sequences that identify the plates from the pools. The barcode is found in the read header and begins with a # and always ends with AT. Therefore, there are 8nt with the first 6 being the index (excluding errors).

                Index sequences:

                name    seq read
                index1  ATCACG
                index2  CGATGT
                index3  TTAGGC
                index4  TGACCA
                index5  ACAGTG
                index6  GCCAAT
                index7  CAGATC
                index8  ACTTGA
                index9  GATCAG
                index10 TAGCTT
                index11 GGCTAC
                index12 CTTGTA



    The assignments of indexes to plates within pools A, B, C and D was as follows:

        Pool    Plate #    indexes

        A     Plate 2    1,2,3
                Plate 3    4,5,6
                Plate 13   7,8,9
                Plate 14   10,11,12

        B     Plate 4    1,2,3
                Plate 5    4,5,6
                Plate 6    7,8,9,
                Plate 7    10,11,12

        C     Plate 8    1,2,3,
                Plate 9    4,5,6
                Plate 10   7,8,9,
                Plate 11   10,11,12

        D     Plate 15    1,2,3
                Plate 16    4,5,6
                Plate 17    7,8,9
                Plate 1    none - so indexes CTTGTA, GGCTAC, and TAGCTT are not found in Pool D headers. Instead, a much wider variety of sequences that don't match any index (even at Hamming distance 1) are found in the header lines. Presumably those reads are from plate 1.

 Each lane must first have the plates separated as each lane has a mix of four plates. A grep command pulls out the reads (grep -EA3) by matching to the 6nt sequences in the header following the # symbol. The sequences from each lane are put into separate folders still separated by their lane pool and forward and reverse reads (8 files in total in each folder). For plate 1, a perl script was written to pull out non-matching  indices but is presumed to have index sequences with errors that may belong to other plates.

 The 4 lanes of the forward were concatenated into a single file. The forward reads were run through FastX toolkit barcode splitter which can only read single ends. For the reverse, a script was created to pull the corresponding reverse reads for each sample from the reverse reads in lanes merged. In each plate, at the end, there should be 96 samples forward and 96 samples reverse. The number of reads in each sample should be similar but rarely perfectly so.

                Example of Fastx command:
                zcat ../reads/forward_reads.fastq.gz | ../../fastx_barcode_splitter.pl --bcfile ../../barcodes.txt --prefix 1_ -bol


        Separating the plates by barcode:The provided barcodes:

        A01     TGACGCCATG
        A02     CAGATATGCA
        A03     GAAGTGTGCA
        A04     TAGCGGATTG
        A05     TATTCGCATT
        A06     ATAGATTGCA
        A07     CCGAACATGC
        A08     GGAAGACATT
        A09     GGCTTATGCA
        A10     AACGCACATT
        A11     GAGCGACATT
        A12     CCTTGCCATT
        B01     GGTATATGCA
        B02     TCTTGGTGCA
        B03     GGTGTTGCAG
        B04     GGATATGCAG
        B05     CTAAGCATGC
        B06     ATTATTGCAG
        B07     GCGCTCATGC
        B08     ACTGCGATTG
        B09     TTCGTTTGCA
        B10     ATATAATGCA
        B11     TGGCAACAGA
        B12     CTCGTCGTGC
        C01     GCCTACCTTG
        C02     CACCATGCAG
        C03     AATTAGTGCA
        C04     GGAACGATGC
        C05     ACAACTTGCA
        C06     ACTGCTTGCA
        C07     CGTGGACAGT
        C08     TGGCACAGAT
        C09     TGCTTTGCAG
        C10     GCAAGCCATT
        C11     CGCACCAATT
        C12     CTCGCGGTGC
        D01     AACTGGTGCA
        D02     ATGAGCAATG
        D03     CTTGATGCAG
        D04     GCGTCCTTGC
        D05     ACCAGGATGC
        D06     CCACTCATGC
        D07     TCACGGAAGT
        D08     TATCATGCAG
        D09     TAGCCAATGC
        D10     ATATCGCCAT
        D11     CTCTATGCAG
        D12     GGTGCACATT
        E01     CTCTCGCATT
        E02     CAGAGGTTGC
        E03     GCGTACAATT
        E04     ACGCGCGTGC
        E05     GTCGCCTTGC
        E06     AATAACCAAT
        E07     AATGAACGAT
        E08     CGTCGCCACT
        E09     ATGGCAATGC
        E10     GAAGCATGCA
        E11     AACGTGCCTT
        E12     CCTCGTGCAG
        F01     CTCATTGCAG
        F02     ACGGTACTTG
        F03     GCGCCGTGCA
        F04     CAAGTTGCAG
        F05     TCCGAGTGCA
        F06     TAGATGATGC
        F07     TGGCCAGTGC
        F08     GCACGATTGC
        F09     TTGCTGTGCA
        F10     CGCAACCAGT
        F11     TCACTGTGCA
        F12     ACAGTTGCAG
        G01     GGAGTCAAGT
        G02     TGAATTGCAG
        G03     CATATTGCAG
        G04     GTGACACATT
        G05     TATGTTGCAG
        G06     CAGTGCCATT
        G07     ACAACCAACT
        G08     TGCAGATGCA
        G09     CATCTGCCGT
        G10     GGACAGTGCA
        G11     ATCTGTTGCA
        G12     AAGACGCTTG
        H01     GAATGCAATA
        H02     TAGCAGTGCA
        H03     ATCCGTGCAG
        H04     CTTAGTGCAG
        H05     TTATTACATT
        H06     GCCAACAAGA
        H07     TGCCGCATTG
        H08     CGTGTCATGC
        H09     CAACCACACA
        H10     GCTCCGATGC
        H11     TCAGAGATTG
        H12     CGTTCATGCA

   These barcodes are variable in length but are filled out to 10bp from the remnant of the PstI restriction site (TGCAG) for fastx barcode splitter (fastx tools). Then, as mentioned before, a personal script removes the reverse matching sample.

