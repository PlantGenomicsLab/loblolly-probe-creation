 # All reads are trimmed with sickle pe (version 1.33). Quality and length thresholds were set to 30 and 50 respectively.

        # Sample command:
        sickle pe \
        -f  ../samples/1_A01.fastq \
        -r ../samples/2_A01.fastq \
        -t illumina \                                                   # Or sanger depending on read type
        -o trimmed_1_A01.fastq \
        -p trimmed_2_A01.fastq \
        -s trimmed_single_A01.fastq \
        -q 30 \
        -l 50
