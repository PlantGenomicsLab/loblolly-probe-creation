**Data sources and QC**
--

Exome capture - Texas A&M University (TAMU)
--

Lu, M., Krutovsky, K. V., Nelson, C. D., Koralewski, T. E., Byram, T. D., & Loopstra, C. A. (2016). Exome genotyping, linkage disequilibrium and population structure in loblolly pine (Pinus taeda L.). BMC genomics, 17(1), 730. https://doi.org/10.1186/s12864-016-3081-8

- **Bioproject**: PRJNA320114
- **Bam files** : https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/sorted_bam_files/TAMU_sorted_bam.tar.gz
- **Strict quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/strict/TAMU_strict_vcf.tar.gz
- **Standard quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/standard/TAMU_standard_vcf.tar.gz


Exome capture - University of Florida (UF)
--

Acosta, J. J., Fahrenkrog, A. M., Neves, L. G., Resende, M., Dervinis, C., Davis, J. M., Holliday, J. A., & Kirst, M. (2019). Exome Resequencing Reveals Evolutionary History, Genomic Diversity, and Targets of Selection in the Conifers Pinus taeda and Pinus elliottii. Genome biology and evolution, 11(2), 508–520. https://doi.org/10.1093/gbe/evz016

- **Bioproject**: PRJNA189744 (SRA for loblolly samples attached as UF_SRA.txt)
- **Bam files** : https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/sorted_bam_files/UF_sorted_bam.tar.gz
- **Strict quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/strict/UF_strict_vcf.tar.gz
- **Standard quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/standard/UF_standard_vcf.tar.gz


ddRAD - North Carolina State University (NCSU)
--

- **Bioproject**: PRJNA453764 (SRA for loblolly samples attached as UF_SRA.txt)
- **Bam files** (16) : https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/sorted_bam_files/NCSU_*_sorted_bam.tar.gz
- **Strict quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/strict/NCSU_strict_vcf.tar.gz
- **Standard quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/standard/NCSU_standard_vcf.tar.gz

For sample names in the vcfs, 1-12 would be A, 13-24 would be B, etc. The order is always 1-96 starting with all As as ordered in the attached barcode list (NCSU_information.txt). 

![](PSSSS_sites.png)
![](NCSU_reads.png)


Virginia Polytech (VAtech)
--

- **Bioproject**: PRJNA451027 (see details in VA_tech_information.txt)
- **Bam files** : https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/sorted_bam_files/VA_sorted_bam.tar.gz
- **Strict quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/strict/VA_strict_vcf.tar.gz
- **Standard quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/standard/VA_standard_vcf.tar.gz

![](VAtech_reads.png)


WGS 
--

De La Torre, A. R., Puiu, D., Crepeau, M. W., Stevens, K., Salzberg, S. L., Langley, C. H., & Neale, D. B. (2019). Genomic architecture of complex traits in loblolly pine. The New phytologist, 221(4), 1789–1801. https://doi.org/10.1111/nph.15535

- **Bioproject**: PRJNA174450 
- **Bam files** : https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/sorted_bam_files/WGS_meg_sorted_bam.tar.gz
- **Strict quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/strict/WGS_strict.vcf.gz
- **Standard quality joint vcf**: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/standard/WGS_standard.vcf.gz


Illimina Infinium array
--

Eckert, A. J., van Heerwaarden, J., Wegrzyn, J. L., Nelson, C. D., Ross-Ibarra, J., González-Martínez, S. C., & Neale, D. B. (2010). Patterns of population structure and environmental associations to aridity across the range of loblolly pine (Pinus taeda L., Pinaceae). Genetics, 185(3), 969–982. https://doi.org/10.1534/genetics.110.115543

- 3029 previously sucessful probes created from uniquely expressed sequence tags 18 loblolly pine individuals. 
- 1181 sucessfully aligned to the loblolly reference genome (v2.01): https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/CTGN/out_1181_sucessful_alignment.txt.gz
-1660 did not align. However, 919 provided enough sequence and were included in the final array. https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/vcf_files/CTGN/out_1660_nonaligners.txt.gz


For more information on sequencing platforms, data sources, and variant retention, see Table 1 in Caballero et al. 2021. 
