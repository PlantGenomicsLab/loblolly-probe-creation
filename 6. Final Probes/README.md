Screening array
----------------

To guide the selection of variants using population-based criteria, a screening phase was executed. To create the screening array, the filtered set of 642,275 variants (file linked in previous section) were scored by Thermo Fisher Scientific through their custom informatic assessment. 58,155 variants were in the “recommended” category, 99,982 variants in the “neutral” category, 475,585 variants in the “not recommended” category, and 8551 variants in the “not possible” category. On the Illumina Infinium array, out of the 3279 markers, 595 were labeled as “recommended,” 588 were labeled as “neutral,” and 2096 were labeled as “not recommended.”

For the screening array A total of 423,695 variants were included on the assay, using a panel of 424 samples (388 diploid samples and 36 haploid megagametophyte samples). 

See Caballero et al. 2021 for details on selecting probes for the screening array and the screening array process. 

A total of 84,845 variants were labeled as “polyhighres,” “nominorhom,” or “callratebelowthreshold” in the Axiom Analysis Suite (Thermo Fisher Scientific, 2020) software. These variants remained candidates for the array. Final filtering for haploid megagametophyte heterozygosity and further filtering is detailed in Caballero et al. 2021. 

84,845 probes post-screening: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/probes/post_screening_array_sites.txt.gz

Final Pita50K array
----------------

In its final form, the production array contained 46,439 unique features (45,197 SNPs and 1242 indels) from 2423 unique individuals. The final number of markers was lower than the number of variants in the original list (50,418) for two reasons: (1) variants with complementary nucleotides require more than one probe, and (2) stochastic production issues during array manufacture resulted in less than 100% of the surface area of the chip being available for probe synthesis. 

The final Pita50K production array contained 36 aligned and 919 non-aligned Illumina Infinium variants. Of the 45,520 probes with reference genome coordinates, 33.9% were annotated in or near genes.

46,439 unique probes on the final array: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/probes/final_list_unqiue_sites_on_Pita50K_array.txt.gz

Original list of 50,418 probes: https://treegenesdb.org/FTP/temp/Pita_Genotyping/Pita_Genotyping_tmp/probes/markers_on_Pita50K_array.csv.gz
