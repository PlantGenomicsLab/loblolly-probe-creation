# Loblolly Probe Creation

Caballero, M., Lauer, E., Bennett, J., Zaman, S., McEvoy, S., Acosta, J., Jackson, C., et al. 2021. [Toward genomic selection in Pinus taeda: Integrating resources to support array design in a complex conifer genome](https://bsapubs.onlinelibrary.wiley.com/doi/10.1002/aps3.11439). Applications in Plant Sciences 9( 6): e11439. 

Supplemental information and code in the creation of the loblolly pine (_Pinus taeda_) axiom genotyping assay (Pita50K). 

High-throughput sequence data, sourced from exome capture, genotype-by-sequence, and whole genome sequencing from 2698 trees across five sequence populations, was analyzed with the improved genome assembly and annotation for the loblolly pine (v2.01). A variant detection, filtering, and probe design pipeline was developed to detect true variants across and within populations.  From 8.27 million variants, a total of 642,275 were evaluated and 423,695 of those were screened across a range-wide population.

The final informatics and screening approach delivered an Axiom array representing 46,439 high confidence variants to the forest tree breeding and genetics community. Based on the annotated reference genome, 34% were located in or directly up or down-stream of genic regions.

Project contact:
Jill L. Wegrzyn, PhD:  jill.wegrzyn@uconn.edu
and Fikret Isik, PhD: fisik@ncsu.edu

Gitlab maintained by Madison Caballero: mc2698@cornell.edu

![](Flowchart.png)
