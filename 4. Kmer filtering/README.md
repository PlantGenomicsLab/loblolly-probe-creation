k-mer mapping
--------------------
Note: These steps are only performed on the exome capture and ddRAD cohorts. 

A challenge in working with the large repetitive genome and genotyping is the potential for off-target alignment. To assess the potential for off-target probe hybridization, probes are broken down into k-mers and aligned to the genome.

For k-mer alignment testing, probes consisting of 35-nt flanks around strict quality variants were generated in reference sequence (71 nt total). Each probe was then split into four 18-mer sequences with the central variant represented twice. If the variant was an indel, only the first reference nucleotide was used. The genome was subsequently parsed with an 18-nt sliding window and matched to the list of probe 18-mers. 

 The 8,271,816 probes are then be split into four 18-mers:

                GTTTTTCTGCAGGTTCCAGGAGAGTCGACTTAGTT     G       CTTCTGGGTCTTTCTACGACCTGTTAATTTGGGGT

                                                        to
                GTTTTTCTGCAGGTTCCA    GGAGAGTCGACTTAGTG     GCTTCTGGGTCTTTCTAC   GACCTGTTAATTTGGGGT
                

33,087,264  k-mers produced but only 24,875,817 were unique. 

Each probe was scored by the sum of alignment counts to the _P. taeda_ genome for each of its 18-mers. Perfect matches to the probe 18-mer database are tallied so that each 18-mer has a corresponding genome mapping rate:

                18-mers:
                GTTTGGGGTTTTCTGCAG                      12
                GTTCCAGGAGAGTCGACA                      108
                ATGTCATTTGCTGGATCT                      1
                ATGTCATTTGCTGGATCT                      4

The 18-mers with the highest mapping rate were simple repeat sequences. The average 18-mer mapped to the genome 14.6 times and the median was 1 match.

- kmer_test.pl : Code that performs the k-mer genome alignment test.

The probes were then scored based on the 4 x 18-mer mapping information. The lowest score possible for a probe is four if each 18-mer comprising the probe maps to the genome once.

-  kmers_to_probes.pl : Takes the k-mer mapping results and scores the probes. 
 
                C4894431_8731   4
                scaffold184659_49477    285
                scaffold224874_60936    6
                scaffold230316_132519   20
                C4298489_9714   146
                scaffold134533_18050    13
                super4433_93390 8
                scaffold210604_3245     4
                scaffold159656_1848     724
                scaffold144153_12345    13
                scaffold211550_10724    20
                scaffold193283_99324    7


        Highest scoring probe:   4,667,554
        Average:                 285.914
        Median:                  8

Variants with a score above the mean of 286 alignments were remove, resulting in a filtered set of 7,744,534 variants from 2608 unique individuals. 

- inform_SNPs_with_kmer_table.pl : Annotates variants as HIGHMAP if their probe scored above the mean of 286. 

See Fig_S1.jpeg for a diagram of this filtering step.
