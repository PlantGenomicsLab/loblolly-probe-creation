#!usr/bin/perl
# Applies the results of the kmer test.

# Run as:
# perl inform_SNPs_with_kmer_table kmer_inform_output.txt Scored_SNPS.txt

#ARGV[0] == the kmer table. It looks like scaff_pos to a number
#ARGV[1] == a scored SNP file

# What will happen is that STRICT variants that map higher than average are given HIGHMAP as their score.
## STEP 1 is going through the table and creating the hash.
open (TABLE, $ARGV[0]);
while ($line = <TABLE>){
        $line =~ s/\s+$//;

        @split_tab = split "\t", $line;
        $ID_to_score{$split_tab[0]} = $split_tab[1];
}
close TABLE;

## STEP 2 is to evaluate the scored SNPS and make the decision. Average is defined as 285.

open (SCORES, $ARGV[1]);
while ($line = <SCORES>){
        $line =~ s/\s+$//;

        @split_line = split "\t", $line;
        $ID = $split_line[0] . "_" . $split_line[1];

        if ($ID_to_score{$ID} > 285){
                $split_line[15] = "HIGHMAP";
        }

        print $split_line[0], "\t",
                $split_line[1], "\t",
                $split_line[2], "\t",
                $split_line[3], "\t",
                $split_line[4], "\t",
                $split_line[5], "\t",
                $split_line[6], "\t",
                $split_line[7], "\t",
                $split_line[8], "\t",
                $split_line[9], "\t",
                $split_line[10], "\t",
                $split_line[11], "\t",
                $split_line[12], "\t",
                $split_line[13], "\t",
                $split_line[14], "\t",
                $split_line[15], "\n";
}
close SCORES;
