#!usr/bin/perl
# Takes the output of the kmer test and scores the original probe
# Run as:
#   perl kmers_to_probes.pl SNP_probes.txt overlap_table.txt

#ARGV[0] == probes
#ARGV[1] == kmers --> atcgcttagctgctagg # (overlap_table.txt)

open (KMERS, $ARGV[1]);
while ($line = <KMERS>){
        $line =~ s/\s+$//;
        @split = split "\t", $line;
        $kmer_to_number{$split[0]} = $split[1];         # Store the kmer results
}

close KMERS;

open (PROBES, $ARGV[0]);
        while ($line = <PROBES>){
                $line =~ s/\s+$//;

                @split = split "\t", $line;             # Essentially running in reverse. Make kmers from the probes and reference the score. 
                @split_left = split "", $split[0];

                $split_left_1 = $split_left[0] . $split_left[1] .
                        $split_left[2] . $split_left[3] .
                        $split_left[4] . $split_left[5] .
                        $split_left[6] . $split_left[7] .
                        $split_left[8] . $split_left[9] .
                        $split_left[10] . $split_left[11] .
                        $split_left[12] . $split_left[13] .
                        $split_left[14] . $split_left[15] .
                        $split_left[16] . $split_left[17];

                $split_left_1 =~ tr/ATGCN/atgcn/;

                $split_left_2 = $split_left[18] . $split_left[19] .
                        $split_left[20] . $split_left[21] .
                        $split_left[22] . $split_left[23] .
                        $split_left[24] . $split_left[25] .
                        $split_left[26] . $split_left[27] .
                        $split_left[28] . $split_left[29] .
                        $split_left[30] . $split_left[31] .
                        $split_left[32] . $split_left[33] .
                        $split_left[34] . $split[1];

                $split_left_2 =~ tr/ATGCN/atgcn/;
                @split_right = split "", $split[2];

                 $split_right_1 = $split[1] .
                        $split_right[0] . $split_right[1] .
                        $split_right[2] . $split_right[3] .
                        $split_right[4] . $split_right[5] .
                        $split_right[6] . $split_right[7] .
                        $split_right[8] . $split_right[9] .
                        $split_right[10] . $split_right[11] .
                        $split_right[12] . $split_right[13] .
                        $split_right[14] . $split_right[15] .
                        $split_right[16];

                $split_right_1 =~ tr/ATGCN/atgcn/;

                 $split_right_2 = $split_right[17] .
                        $split_right[18] . $split_right[19] .
                        $split_right[20] . $split_right[21] .
                        $split_right[22] . $split_right[23] .
                        $split_right[24] . $split_right[25] .
                        $split_right[26] . $split_right[27] .
                        $split_right[28] . $split_right[29] .
                        $split_right[30] . $split_right[31] .
                        $split_right[32] . $split_right[33] .
                        $split_right[34];

                $split_right_2 =~ tr/ATGCN/atgcn/;

                $INPUT = $INPUT + 4;

                $scaffpos = $split[3] . "_" . $split[4];
                # Score:
                $scaffpos_to_map_count{$scaffpos} =  $kmer_to_number{$split_left_1} + $kmer_to_number{$split_left_2} +  $kmer_to_number{$split_right_1} + $kmer_to_number{$split_right_2};
        }

close PROBES;
 
open (GENOME, ">probe_maps.txt") 
foreach $scaff_pos (keys %scaffpos_to_map_count){
        print $scaff_pos, "\t", $scaffpos_to_map_count{$scaff_pos}, "\n";
}
close GENOME;

open (TABLE, "probe_maps.txt");
        while ($line = <TABLE>){
                $line =~ s/\s+$//;

                @split = split "\t", $line;
                if ($split[0] =~ /./){
                        @split_ = split "_", $split[0];
                        $split_scaff_pos = $split_[0] . "_" . $split_[1];
                        $scaff_pos_to_count{$split_scaff_pos} =  $scaff_pos_to_count{$split_scaff_pos} + $split[1];
                        print $split[1], "\t", $split[0], "\n";
                }
        }
close TABLE;

